package nl.utwente.di.bookQuote;
import java.util.HashMap;

public class Quoter {
	public double getBookPrice(String price) {
		HashMap<String, Double> map = new HashMap<String, Double>();
		map.put("5", 50.0);
		map.put("4", 35.0);
		map.put("3", 20.0);
		map.put("2", 45.0);
		map.put("1", 10.0);
		if (price.equals("1")) {
			return map.get("1");
		} else if (price.equals("2")) {
			return map.get("2");
		} else if (price.equals("3")) {
			return map.get("3");
		} else if (price.equals("4")) {
			return map.get("4");
		} else if (price.equals("5")) {
			return map.get("5");
		} else {
			return 0.0;
		}
		

	}
}
